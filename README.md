# GitOps



![gitops](static/GitOps.png)

![Compoenents on Google Cloud](static/google-components.png)

![Principles of GitOps](static/principles.png)

Config sync continuosly reconciles clusters to a central set of configs stored in Git repositories.
* apply common congiguration with an auditabel, trabnsactional and version controlled deployment process that can span hybrid and multi-cloud.



![Config Sync Overview](static/config-sync.png)

![Typical Setup](static/typical-setup.png)


Value of config-sync when operating at scale
* App developers focus on application logic.
* Platform teams develop and manage config to deploy applications.
* Security admins develop and manage policies. 
* Config, app code and policies are managed using Gitops.
* Easily integrated with existing CI/CD pipelines. 


![Config and Policy sync](static/config-policy-sync.png)




## Usage

* Create GKE Autopilot cluster
  * install `config sync` from GKE Enterprise [view](https://console.cloud.google.com/kubernetes/overview/enterprise)





Examples can be found [here](https://github.com/GoogleCloudPlatform/anthos-config-management-samples).

## Support

## Roadmap

## Contributing


## Authors and acknowledgment

## License

## Project status
